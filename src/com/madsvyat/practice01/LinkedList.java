package com.madsvyat.practice01;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 */
public class LinkedList<T> implements List<T> {

    private static class Node<T> {

        private T data;
        private Node<T> next;
        private Node<T> prev;

        private Node(T data, Node<T> next, Node<T> prev) {
            this.data = data;
            this.next = next;
            this.prev = prev;
        }
    }

    private Node<T> head;
    private Node<T> tail;
    private int size;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean add(T value) {
        Node<T> newNode = new Node<>(value, null, tail);
        if (tail != null) {
            tail.next = newNode;
            tail = newNode;
        }
        if (isEmpty()) {
            head = newNode;
            tail = newNode;
        }
        size++;
        return true;
    }

    @Override
    public void add(int index, T element) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        if (index == 0) {
            Node<T> newNode = new Node<>(element, head, null);
            head.prev = newNode;
            head = newNode;
        } else {
            int position = 1;
            Node<T> oldNode = head.next;
            while (position != index) {
                oldNode = oldNode.next;
                position++;
            }
            Node<T> newNode = new Node<>(element, oldNode, oldNode.prev);
            oldNode.prev.next = newNode;
            oldNode.prev = newNode;

        }
        size++;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        } else if (index == 0) {
            return head.data;
        } else if (index == size - 1) {
            return tail.data;
        } else {
            Node<T> currentNode = head;
            int position = 0;
            while (position != index) {
                currentNode = currentNode.next;
                position++;
            }
            return currentNode.data;
        }
    }

    @Override
    public T set(int index, T element) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        } else if (index == 0) {
            T oldElement = head.data;
            head.data = element;
            return oldElement;
        } else if (index == size - 1) {
            T oldElement = tail.data;
            tail.data = element;
            return oldElement;
        } else {
            int position = 1;
            Node<T> currentNode = head.next;
            while (position != index) {
                currentNode = currentNode.next;
                position++;
            }
            T oldValue = currentNode.data;
            currentNode.data = element;
            return oldValue;
        }
    }

    @Override
    public int indexOf(Object o) {
        if (isEmpty()) {
            return -1;
        }

        int index = 0;
        Node<T> currentNode = head;
        do {
            if (currentNode.data.equals(o)) {
                return index;
            }
            currentNode = currentNode.next;
            index++;
        } while (index < size);

        return -1;
    }

    @Override
    public T remove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        if (index == 0) {
            T removedElement = head.data;
            head.next.prev = null;
            head = head.next;
            size--;
            return removedElement;
        } else if (index == size - 1) {
            T removedElement = tail.data;
            tail.prev.next = null;
            size--;
            return removedElement;
        } else {
            int position = 1;
            Node<T> currentNode = head.next;
            while (position != index) {
                currentNode = currentNode.next;
                position++;
            }
            T removedElement = currentNode.data;
            currentNode.prev.next = currentNode.next;
            currentNode.next.prev = currentNode.prev;
            size--;
            return removedElement;
        }
    }

    @Override
    public boolean remove(Object o) {
        int index = indexOf(o);
        if (index ==  -1) {
            return false;
        } else {
            remove(index);
            return true;
        }
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    @Override
    public void clear() {
        if (!isEmpty()) {
            Node<T> currentNode = head;
            while (currentNode != null) {
                currentNode.prev = null;
                currentNode = currentNode.next;
                size--;
            }
            head = null;
        }
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object element : c) {
            if (!contains(element)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean modified = false;
        for (T element : c) {
            modified = add(element);
        }
        return modified;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean modified = false;
        for (Object element : c) {
             if (remove(element)) {
                 modified = true;
             }
        }
        return modified;
    }

    @Override
    public Iterator<T> iterator() {
        throw new UnsupportedOperationException("not implemented yet");
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("not implemented yet");
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        throw new UnsupportedOperationException("not implemented yet");
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        throw new UnsupportedOperationException("not implemented yet");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("not implemented yet");
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("not implemented yet");
    }

    @Override
    public ListIterator<T> listIterator() {
        throw new UnsupportedOperationException("not implemented yet");
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        throw new UnsupportedOperationException("not implemented yet");
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("not implemented yet");
    }

}
