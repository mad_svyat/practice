package com.madsvyat.practice01;

import java.util.List;

public class Main {

    public static void main(String[] args) {


        LinkedList<Integer> list = new LinkedList<>();
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(78);
        printList(list);
        System.out.println("list size: " + list.size());
        System.out.println("element #1: " + list.get(1));
        list.set(1, 88);
        list.add(42);
        printList(list);
        System.out.println("element #1: " + list.get(1));
        System.out.println("index of 78: " + list.indexOf(78));
        printList(list);
        list.remove(0);
        printList(list);
        list.remove(2);
        list.remove(1);
        printList(list);
        list.add(3909);
        list.add(23);
        printList(list);
        list.add(0, 55);
        list.add(1, 908);
        list.add(5, 409);
        printList(list);
        list.clear();
        System.out.println("list size after clear: " + list.size());
    }

    private static <T> void printList(List<T> list) {
        System.out.print("[ ");
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i));
            if (i < list.size() - 1) {
                System.out.print(", ");
            }
        }
        System.out.print("]\n");
    }
}
